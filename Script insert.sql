USE DBSISTEMAS_M


INSERT INTO ROL
VALUES
	  ('Programador'),
	  ('Dise�ador de intefaces'),
	  ('DBA');
GO
SELECT * FROM ROL
GO

INSERT INTO PLATAFORMA
VALUES
		('Web'),
		('Escritorio');
GO
SELECT * FROM PLATAFORMA
GO;

INSERT INTO ESTADO
VALUES 
		('Pendiente'),
		('En desarrollo'),
		('Mantenimiento'),
		('Finalizado');
GO
SELECT * FROM ESTADO
GO

INSERT INTO ROL_USUARIO
VALUES
	('Administrador'),
	('Usuario regular');
GO
SELECT* FROM ROL_USUARIO
GO

INSERT INTO RESPONSABLE
VALUES
	('Alex'),
	('Edith'),
	('Ever'),
	('Jona'),
	('Ra�l'),
	('Eduardo'),
	('Irma'),
	('Jes�s'),
	('Bryan'),
	('Jorge');
GO
SELECT * FROM RESPONSABLE
GO

INSERT INTO SISTEMA
VALUES
	(1,2,'Vales'),
	(1,2,'Capacitaci�n'),
	(2,3,'Minutas'),
	(2,4,'Ponencias'),
	(2,1,'Extensiones'),
	(1,3,'Ingresos'),
	(1,4,'Interfaces'),
	(2,2,'Tarjetas'),
	(1,1,'Oficinas'),
	(2,3,'Regalos');
GO
SELECT * FROM SISTEMA
GO


INSERT INTO ROL_RESPONSABLE
VALUES
		(3,2,3),
		(2,5,10),
		(1,4,8),
		(1,1,1),
		(2,6,2),
		(1,3,7),
		(3,10,6),
		(3,8,4),
		(2,7,9),
		(1,9,5);
GO
SELECT * FROM ROL_RESPONSABLE
GO

INSERT INTO DEPARTAMENTO
VALUES
	(1,'Cafeteria'),
	(10,'Papeleria'),
	(2,'Desarrollo'),
	(9,'�rea administrativa'),
	(3,'Recursos humanos'),
	(8,'Sociedad de alumnos'),
	(4,'Centro de lenguajes'),
	(7,'Mantenimiento'),
	(5,'Seguridad'),
	(6,'Tesoreria');
GO
SELECT * FROM DEPARTAMENTO
GO

INSERT INTO USUARIO
VALUES
		(1,'Edith'),
		(2,'Ever'),
		(2,'Alex'),
		(2,'Ra�l'),
		(2,'Jona'),
		(2,'Carlos'),
		(2,'Jose'),
		(2,'Mariano'),
		(2,'Karla'),
		(1,'Juanito');
GO
SELECT * FROM USUARIO
GO

