USE DBSIGEF
GO

--Muestra los conceptos de pago que su moneda sea en euros y en dolares, adem�s que su fecha de vigencia sea el mes de octubre
SELECT * 
FROM CONCEPTO
WHERE Descripcion LIKE '%EUR%' OR Descripcion LIKE '%USD%' AND
	  FechaFinal BETWEEN '1/10/2021' AND '31/10/2021';
GO

--Muestra las boletas que su referencia contenga "1179" y que su fecha de revisi�n no sea nula
SELECT * 
FROM BOLETA
WHERE Referencia LIKE '1179%' AND
	 FechaRevision IS NOT NULL;
GO

--MUESTRA LOS USUARIOS QUE SU CORREO SEA UNA CUENTA GMAIL Y EN EL CAMPO P�is no sea un dato nulo
SELECT * 
FROM USUARIO
WHERE Email LIKE '%gmail%' AND
		Pais IS NOT NULL;
GO

--Muestra a los usaurios que son doctores y que su rol sea "participantes"
SELECT *
FROM USUARIO 
WHERE Prefijo LIKE '%Dr%' AND
	  IdRol=4;
GO

--Busca tu usuariro con rol de participante y muestra las ponencias que realizaste que fueron aprobadas
SELECT * 
FROM PONENCIA
WHERE IdUsuario=(SELECT IdUsuario
				 FROM USUARIO
				 WHERE 	Nombre LIKE '%Eduardo%' AND
						IdRol= (SELECT IdRol 
								FROM ROL
							    WHERE Descrip='Participante') ) AND
	 IdEstatus=(SELECT IdEstatus
				FROM ESTATUS
				WHERE Descrip='Approved');
GO


--Muestra los usuarios que son evaluadores (rol comite) de la linea "Finance and Economy" 
SELECT *
FROM USUARIO
WHERE IdUsuario IN (SELECT IdUsuario --Le pongo IN por que nos va a traer mas de un usuario de comite
				   FROM USUARIO_COMITE
				    WHERE IdLinea = (SELECT IdLinea --Aqu� un igual, por que solo estoy buscando una linea, y me va a traer una sola
								      FROM LINEA
								      WHERE Descripcion='Finance and Economy'));
GO
----------------------------------------------------------------------------------------------------------------------------------------


--BOLETA: Muestra el promedio de monto pagado pro los participantes de SIGEF que han realizado un pago en una moneda mexicana (MXN)

SELECT  AVG(MONTO) AS 'Monto promedio'
FROM	BOLETA
WHERE  Concepto LIKE '%MXN%';
GO

--BOLETA: Muestra la cantidad total pagada por concepto de pago, ordenalos por orden alfab�toc
SELECT SUM(Monto) AS 'Total',
	   Concepto
FROM   BOLETA 
GROUP BY Concepto
ORDER BY Concepto ASC;
GO

--Ponencia, estatus y linea: Muestra la cantidad de ponencias que su estatus es approved y pertenezcan a la linea FAE

SELECT COUNT(IdPonencia) AS 'Cantidad ponencias'
FROM PONENCIA 
WHERE IdEstatus=(SELECT IdEstatus
				 FROM   ESTATUS
				 WHERE Descrip='Approved') AND
	  IdLinea=(SELECT IdLinea
				FROM LINEA
				WHERE Descripcion='Finance and Economy');
GO

--CONCEPTO: En una sola consulta muestra el monto m�nimo y el monto m�ximo de montos que se encuentran disponibles para los miembros del SIGEF
SELECT MIN(Monto) AS 'Monto minimo',
	  MAX(Monto) AS 'Monto m�ximo'
FROM CONCEPTO;
 GO;


 ----------------------------------------------------------------------------------------
 --Muestra las ponencias que pasaron por na segunda revisi�n y fueron aprovadas
 SELECT *
 FROM PONENCIA
 WHERE IdPonencia IN (SELECT IdPonencia
					 FROM PONENCIA_BIT
					 WHERE IdEstatus= (SELECT IdEstatus
										FROM ESTATUS
										WHERE Descrip='Second Review')) AND
		IdEstatus=(SELECT IdEstatus
				   FROM ESTATUS
				   WHERE Descrip='Approved');
GO

--Subconsulta, muestra el idponencia, el nombre de la linea a la que pertence la ponencia, el tema y la descripcion de la ponencia
SELECT PON.IdPonencia,
	   (SELECT LIN.Descripcion FROM LINEA AS LIN WHERE PON.IdLinea=LIN.IdLinea) AS LINEA,
	   PON.Tema,
	   PON.Descripcion
FROM PONENCIA AS PON;
GO

------Muestra las ponencias que fueron fechazadas desde su primer revisi�nW
SELECT *
FROM PONENCIA
WHERE IdPonencia IN (SELECT IdPonencia
					 FROM PONENCIA_ARCHIVO
					 GROUP BY IdPonencia
					 HAVING COUNT(IdPonencia)=2) AND
	IdEstatus=7;
GO


-----USUARIO_COMITE, USUARIO Y LINEA: Subconsulta, muestra el nombre de la linea a la que fue asignado cada evaluador y en otra columna muestra el nombre del evaluador, 
--ordenalos por linea y por nombre, en orden alfabetico (A a la Z)

SELECT  Linea=(SELECT Descripcion
			FROM LINEA AS LIN
			WHERE USC.IdLinea=LIN.IdLinea),
		Nombre=(SELECT Nombre
			   FROM USUARIO AS USU
			   WHERE USU.IdUsuario=USC.IdUsuario)
FROM USUARIO_COMITE AS USC
ORDER BY Linea ASC, Nombre ASC;
GO

----------------------------------------------------------------------------------------------

---Consulta exigente

SELECT SUM(BOL.Monto) AS 'Monto Pagado',
		(SELECT IdUsuario FROM USUARIO AS USU WHERE USU.IdUsuario=BOL.IdUsuario) AS 'Folio',
		NombreCompleto=CONCAT((SELECT Nombre FROM USUARIO AS USU WHERE USU.IdUsuario=BOL.IdUsuario),' ',(SELECT Apellidos FROM USUARIO AS USU WHERE USU.IdUsuario=BOL.IdUsuario))
FROM BOLETA AS BOL
WHERE BOL.IdEstatus=(SELECT IdEstatus
					  FROM ESTATUS AS EST
					   WHERE EST.Descrip='Approved') AND
	 BOL.FechaRevision BETWEEN '1/10/2021' AND '30/11/2021'
GROUP BY BOL.IdUsuario
HAVING SUM(BOL.Monto)>=250
ORDER BY NombreCompleto

--Muestra el idusuario y nombre completo de los usuarios que subieron la mayor cantidad de ponencias
SELECT IdUsuario ,
		NombreCompleto=CONCAT((SELECT Nombre FROM USUARIO AS USU WHERE USU.IdUsuario=PON.IdUsuario),' ',(SELECT Apellidos FROM USUARIO AS USU WHERE USU.IdUsuario=PON.IdUsuario)) 
FROM PONENCIA AS PON
WHERE IdUsuario IN (SELECT  IdUsuario
					FROM PONENCIA
					GROUP BY IduSUARIO
					HAVING COUNT(IdPonencia)=(SELECT  MAX(totalPon)
											  FROM ( SELECT  IdUsuario,
													 COUNT(IdPonencia) AS totalPon
													 FROM PONENCIA  
													 GROUP BY IdUsuario) AS s))
GROUP BY IdUsuario




SELECT IdUsuario ,
		(SELECT Nombre + ' ' + Apellidos FROM USUARIO AS USU WHERE PON.IdUsuario = USU.IdUsuario) AS NombreCompleto
FROM PONENCIA AS PON
GROUP BY IdUsuario
HAVING COUNT(IdPonencia)=(SELECT  MAX(totalPon)
							FROM ( SELECT  IdUsuario,
									COUNT(IdPonencia) AS totalPon
									FROM PONENCIA  
									GROUP BY IdUsuario) AS s)
