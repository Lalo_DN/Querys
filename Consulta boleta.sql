USE DBSIGEF
GO
SELECT * FROM USUARIO
SELECT * FROM USUARIO_COMITE
	SELECT * FROM BOLETA

SELECT BOL.IdBoleta AS 'No. Boleta',
		UPPER(EST.Descrip) AS Estatus,
		BOL.Concepto,
		(USU.Nombre+' '+USU.Apellidos) AS Nombre,
		CASE
			WHEN BOL.Concepto LIKE '%MXN%' THEN FORMAT(BOL.Monto,'C','es-MX')+' MXN'
			WHEN BOL.Concepto LIKE '%EUR%' THEN FORMAT(BOL.Monto,'C','fr-FR')+ 'EUR' 
			WHEN BOL.Concepto LIKE '%USD%' THEN FORMAT(BOL.Monto,'C','en-us')+ 'USD'
		END AS Concepto,
		BOL.Referencia,
		CONVERT(NVARCHAR,BOL.FechaVencimiento,103) AS Vencimiento,
		CASE
			WHEN BOL.Comprobante IS NULL THEN 'Sin comprobante'
			ELSE BOL.Comprobante
		END AS Comprobante,
		CASE
			WHEN BOL.FechaRevision IS NULL THEN 'Sin revisi�n'
			ELSE CONVERT(nvarchar,BOL.FechaRevision,103)
		END AS FechaRevision,
FROM BOLETA AS BOL
	INNER JOIN ESTATUS AS EST ON BOL.IdEstatus=EST.IdEstatus
	INNER JOIN USUARIO AS USU ON BOL.IdUsuario=USU.IdUsuario