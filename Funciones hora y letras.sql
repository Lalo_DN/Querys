USE DBDULCERIA

SELECT * FROM CATEGORIA

SELECT CAT.IdCategoria,
	   TIP.Descripcion,
	   CAT.Descripcion,
	   CONCAT(TIP.Descripcion,' ',CAT.Descripcion) AS HOLA
FROM CATEGORIA AS CAT
	LEFT JOIN TIPO_PRODUCTO AS TIP ON CAT.IdTipoProducto = TIP.IdTipoProducto;

DECLARE @v_Ejemplo NVARCHAR(30)
SET @v_Ejemplo='      HolA      '
SELECT TRIM(@v_Ejemplo) AS 'Trim',
	   LTRIM(@v_Ejemplo) AS 'Ltrim',
	   RTRIM(@v_Ejemplo) AS 'Rtrim',
	   UPPER(TRIM(@v_Ejemplo)) AS 'Upper',
	   LOWER(TRIM(@v_Ejemplo)) AS 'Lower',
	   LEN(@v_Ejemplo) AS 'LEN';
GO