USE DBSIGEF
GO

------Muestra el id, nombre completo y l�nea que eval�an cada evaluador del comit�. Ordenalos alfabeticamente
SELECT USC.IdUsuarioComite,
       USU.Nombre + ' ' + USU.Apellidos AS Nombre_completo,
	   LIN.Descripcion AS Nombre_linea
FROM USUARIO_COMITE AS USC INNER JOIN USUARIO AS USU ON USC.IdUsuario=USU.IdUsuario
							INNER JOIN LINEA AS LIN ON USC.IdLinea=LIN.IdLinea
ORDER BY Nombre_completo ASC;
GO

--Muestra el folio (id), linea, estatus, usuario creador de la ponencia y el tema de todas las ponencia sin importar
--su estatus (para la l�nea, estatus y usuario deber�n mostrar las descripciones/nombres y no los ids)

SELECT PON.IdUsuario AS Folio,
	   LIN.Descripcion AS L�nea,
	   EST.Descrip AS Estatus,
	   USU.Nombre+' '+USU.Apellidos AS Nombre_completo,
	   PON.Tema
FROM PONENCIA AS PON 
	 LEFT JOIN ESTATUS  AS EST ON PON.IdEstatus=EST.IdEstatus
	 INNER JOIN USUARIO AS USU ON PON.IdUsuario=USU.IdUsuario
	 INNER JOIN LINEA   AS LIN ON PON.IdLinea=LIN.IdLinea;
GO

--Muestra el id, estatus, rol c�digo de autenticaci�n y usuario sin importar si realiz� o no autenticaci�n de cuenta ( para rol, c�digo y estatus)
--deber�n de mostrar las descripciones/nombres y no los ids) Ordenalos por rol, estatus y nombre de la A la Z
SELECT USU.IdUsuario,
	   EST.Descrip AS Estado,
	   ROL.Descrip AS Rol,
	   COD.Codigo,
	   USU.Nombre + ' ' + USU.Apellidos AS Usuario
FROM USUARIO AS USU 
	 LEFT JOIN  CODIGO   AS COD ON USU.IdCodigo = COD.IdCodigo
	 INNER JOIN ESTATUS  AS EST ON USU.IdEstatus = EST.IdEstatus
	 INNER JOIN ROL            ON USU.IdRol = ROL.IdRol
ORDER BY Usuario ASC;
GO


--En una consulta, muestra las posibles evaluadores de cada ponencia
SELECT PON.IdPonencia AS Folio,
	   PON.Tema,
	   LIN.Descripcion AS 'L�nea de revisi�n',
	   USC.IdUsuarioComite
FROM PONENCIA AS PON 
	 INNER JOIN LINEA AS LIN ON PON.IdLinea=LIN.IdLinea
	 CROSS JOIN USUARIO_COMITE AS USC
WHERE USC.IdLinea=PON.IdLinea



				  



--Me treae id de usuario y la linea que administra
select usc.IdUsuarioComite,lin.IdLinea from usuario_comite as usc join linea as lin on usc.IdLinea=lin.IdLinea
--Me trae la ponencia y la linea a la que corresponde
select pon.IdPonencia,lin.IdLinea from ponencia as pon join linea as lin on pon.IdLinea=lin.IdLinea
