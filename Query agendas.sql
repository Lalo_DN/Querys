/*	#####################		HISTORIAL DE VERSIONES			###################
	
	[--------------------------------------------------------------------------------]
	 VERSI�N DE LA BASE 1.1.0
	 FECHA DE MODIFICACI�N: 16/11/2021 11:00 AM

	 Cambios realizados en la base de datos:
	 Ninguna
	 
	 Cambios realizados en las tablas:
	-Se movi� la llave foranea genero de USUARIO a POBLACION_UNIVERSITARIA
	-Se elimin� la propiedad NOT NULL de FechaAsignacion de la tabla CUPON
	-Se elimin� la propiedad NOT NULL de FechaCanjeado de la tabla CUPON_CANJEADO
	-Se agregaron los campos de estado y municipios 
	-Se dividi� el campo Apellidos a ApellidoMat y ApellidoPat en la tabla CONTACTO
	-Se elimin� la porpiedad NOT NULL de Foto de la tabla CONTACTO
	-Se dividi� el campo Apellidos a ApellidoMat y ApellidoPat en la tabla POBLACION_UNIVERSITARIA

	Cambios en los storedprocedures:
	Ninguna

	[--------------------------------------------------------------------------------]

	VERSI�N DE LA BASE 1.1.0.1
	FECHA DE MODIFICACI�N: 17/11/2021 11:10 AM

	Correciones realizadas:
	-Se cambi� una FK que apuntaba a la primary key de la misma entidad en la tabla TELFONO

	[--------------------------------------------------------------------------------]
	VERSI�N DE LA BASE 1.2.0
	FECHA DE MODIFICACI�N: 19/11/2021	9:15 AM

	Cambios realizados en la base de datos:
	Ninguna

	Cambios realizados en las tablas:
	-Se elimin� la relaci�n de la tabla ESTATUS con CORREO y TELEFONO
	-Se agreg� la foreign key de ESTATUS a la tabla CONTACTO
	-Se elimin� la relaci�n de la tabla ESTATUS con POBLACION_UNIVERSITARIA
	-Se agreg� la foreign key de ESTATUS a la tabla USUARIO
	-Se agreg� la foreign key de ESTATUS a la tabla EVENTO
	-Los campos que pod�an tener datos nulos, ahora tienen la propiedad NULL

	Cambios en los storedprocedures:
	Ninguna

	[--------------------------------------------------------------------------------]
	VERSI�N DE LA BASE 1.3.0
	FECHA DE MODIFICACI�N: 19/11/2021	11:00 AM

	Cambios realizados en la base de datos:
	Ninguna

	Cambios realizados en las tablas:
	-Se agreg� la relaci�n de la tabla PAIS con ESTADO
	-Se agrand� la capacidad del campo de Descripcion en la tabla ESTADO (De 30 a 40 caracteres)
	-Se agrand� la capacidad del campo de Descripcion en la tabla MUNICIPIO (De 40 a 80 caracteres)

	Cambios en los storedprocedures:
	Ninguna
	[--------------------------------------------------------------------------------]
	VERSI�N DE LA BASE 1.4.0.1
	FECHA DE MODIFICACI�N: 22/11/2021 10:45 AM
	
	Cambios realizados en la base de datos:
	Ninguna

	Cambios realizados en las tablas:
	-Se elimin� la relaci�n de ESTATUS con la tabla CONTACTO
	-Se alter� la propiedad de la columna Correo en la tabla USUARIO para que fuera nulo
	-Se agreg� la columna NombreUsuario a la tabla USUARIO de tipo NVARCHAR(30) y NULL
	-La columna "FechaIngreso" se renombr� por "FechaEgreso" en la tabla POBLACION_UNIVERSITARIA y se permiti� que fuera nulo
	-Se cambi� el tipo de dato de INT a NVARCHAR(25) en la columna NoIdentificador en la tabla POBLACION_UNIVERSITARIA
	
	Cambios en los stored procedures:
	Ninguna

	Otros cambios:
	-Se agregaron los valores de las tablas cat�logo (Ver script)
	[--------------------------------------------------------------------------------]
	VERSI�N DE LA BASE 1.5.1
	FECHA DE MODIFICACI�N 25/11/2021 10:40 AM
	
	Cambios realizados en la base de datos:
	Ninguna

	Cambios realizados en las tablas:
	-Se movi� la llave foranea de la tabla POBLACION_UNIVERSITARIA hac�a la tabla USUARIO
	-Se agreg� la columna FechaEgreso a la tabla USUARIO de tipo DATE
	-Se elimin� la columna y la foreign key de IdEstatus de la tabla CONTACTO

	Cambios en los stored procedures:
	-Agregado el spUSUARIO
	-Agregado el spCONTACTO

	[--------------------------------------------------------------------------------]

	VERSI�N DE LA BASE 1.6.1
	FECHA DE MODIFICACI�N 26/11/2021 11:15 AM
	
	Cambios realizados en la base de datos:
	Ninguna

	Cambios realizados en las tablas:
	-Se agreg� la columna FechaRegistro en la tabla CONTACTO
	-Se modific� la propiedad NOT NULL de la columna FechaNac en la tabla CONTACTO

	Cambios en los stored procedures:
	-Se termin� el spCONTACTO con 7 acciones

	[--------------------------------------------------------------------------------]

	VERSI�N DE LA BASE 1.7.1
	FECHA DE MODIFICACI�N 1/12/2021 11:55 AM

	Cambios realizados en la base de datos:
	Ninguna

	Cambios realizados en las tablas:
	-Se cambi� el nombre de la columna "Telefono" a "Numero" en la tabla TELEFONO

	Cambios en los stored procedures:
	-Se cre� el spCORREO con 6 acciones
	-Se cre� el spTELEFONO con 6 acciones
	-Se cre� el spCOMBOS con 11 acciones
	
	[--------------------------------------------------------------------------------]

	VERSI�N DE LA BASE 1.8.1
	FECHA DE MODIFICACI�N 2/12/2021 11:40 AM

	Cambios realizados en la base de datos:
	Ninguna

	Cambios realizados en las tablas:
	-Se elimin� la foreign key FK_IdEstatus_Evento de la tabla EVENTO
	-Se elimin� la columna IdEstatus de la tabla EVENTO
	-Se agerg� la propiedad DELETE ON CASCADE a las FK de las tablas CORREO y TELEFONO que apuntan a CONTACTO

	Cambios realizados en los stored procedures:
	-Se agreg� una clausla WHERE al combo de Estado en spCOMBOS
	-Se agreg� una clausula WHERE al combo de Municipio en spCOMBOS
	-Se cre� el spEVENTO con 5 acciones
		
	[--------------------------------------------------------------------------------]

	VERSI�N DE LA BASE 1.8.2 
	FECHA DE MODIFICACI�N 8/12/2021 11:45 AM
	Cambios realizados en la base de datos:
	Ninguna

	Cambios realizados en als tablas:
	Ninguna

	Cambios realizados en los stored procedures:
	-Se ha a�adido una nueva acci�n al spUSUARIO el cu�l me trae el nombre de un usuario de la tabla POBLACION_UNIVERSITARIA
	-Se ha creado el spINFORMACION con 5 acciones
	-Se ha creado el spCUPON con 6 acciones

	[--------------------------------------------------------------------------------]

	VERSI�N DE LA BASE 1.8.3
	FECHA DE MODIFICACI�N 10/12/2021 11:53 AM

	Cambios realizados en la base de datos:
	Ninguna

	Cambios realizados en las tablas:
	Ninguna

	Cambios realizados en los stored procedures:
	-La acci�n 3 del spEVENTO se ha simplificado

	
	[--------------------------------------------------------------------------------]

	VERSI�N DE LA BASE 1.9.1
	FECHA DE MODIFICACI�N 13/12/2021 12:00 PM

	Cambios realizados en la base de datos:
	Ninguna

	Cambios realizados en las tablas:
	-Se ha agregado la columna de FechaRegistro a la tabla CONTACTO
	-Se ha cambiado la propiedad de la columna FechaNac de la tabla CONTACTO a null

	Cambios realizados en los stored procedures:
	-Se ha agregado una quinta acci�n al spEvento
	-Se ha terminado hasta la acci�n #8 en el spCupon
	[--------------------------------------------------------------------------------]

	VERSI�N DE LA BASE 1.9.2
	FECHA DE MODIFICACI�N 14/12/2021 12:00 PM
	
	Cambios realizados en la base de datos:
	Ninguna

	Cambios realizados en las tablas:
	Ninguna

	Cambios realizados en los stored procedures:
	-Se han agregado varias condiciones de IdUsuario=@IdUsuario en el spCONTACTO por seguridad.
	-La acci�n 6 de spCONTACTO se ha modificado para traer los nombres por separado en lugar de un solo campo con el nombre completo.
	-La acci�n 4 del spCONTACTO ahora trae todos los campos del contacto en lugar de solo su id
	[--------------------------------------------------------------------------------]
	VERSI�N DE LA BASE 1.9.3
	FECHA DE MODIFICACI�N 16/12/2021 11:07 PM
	
	Cambios realizados en la base de datos:
	Ninguna

	Cambios realizados en las tablas:
	Ninguna

	Cambios realizados en los stored procedures:
	-La accion 1 del spCONTACTO ahora trae el id con el que fue modificado
	[--------------------------------------------------------------------------------]



	################################################################################
*/


--BASE DE DATOS DEL PROYECTO
CREATE DATABASE DBAGENDA_M
USE DBAGENDA_M;
GO

--Rol del usuario
CREATE TABLE [dbo].[ROL]
(
	IdRol		 INT IDENTITY(1,1)	NOT NULL,
	Descripcion  NVARCHAR(25)		NOT NULL,
	CONSTRAINT PK_IdRol PRIMARY KEY (IdRol)
);
GO

--Estatus que pega a usuario y cup�n
CREATE TABLE [dbo].[ESTATUS]
(
	IdEstatus	 INT IDENTITY(1,1)	NOT NULL,
	Descripcion  NVARCHAR(15)	    NOT NULL,
	CONSTRAINT PK_IdEstatus PRIMARY KEY (IdEstatus)
);
GO
--G�nero del usuario (masculino, femenino y sin especificar)
CREATE TABLE [dbo].[GENERO]
(
	IdGenero	 INT IDENTITY(1,1)	   NOT NULL,
	Descripcion  NVARCHAR(15)			NOT NULL,
	CONSTRAINT PK_IdGenero PRIMARY KEY (IdGenero)
);
GO

--Tabla cat�logo de tipos de correo
CREATE TABLE [dbo].[TIPO_CORREO]
(
	IdTipoCorreo	INT IDENTITY(1,1)	NOT NULL,
	Descripcion		NVARCHAR(30)		NOT NULL,
	CONSTRAINT PK_IdTipoCorreo PRIMARY KEY(IdTipoCorreo)
);
GO

--Tabla cat�logo de tipos de tel�fono
CREATE TABLE [dbo].[TIPO_TELEFONO]
(
	IdTipoTelefono	INT IDENTITY(1,1)	NOT NULL,
	Descripcion		NVARCHAR(30)		NOT NULL,
	CONSTRAINT PK_IdTipoTelefono PRIMARY KEY(IdTipoTelefono),
);
GO

--Tabla cat�logo de tipos de contacto
CREATE TABLE [dbo].[TIPO_CONTACTO]
(
	IdTipoContacto  INT IDENTITY(1,1)	NOT NULL,
	Descripcion		NVARCHAR(30)		NOT NULL,
	CONSTRAINT PK_IdTipoContacto PRIMARY KEY(IdTipoContacto),
);
GO

--Tabla cat�logo de tipos de evento
CREATE TABLE [dbo].[TIPO_EVENTO]
(
	IdTipoEvento	INT IDENTITY(1,1)	NOT NULL,
	Descripcion		NVARCHAR(30)		NOT NULL,
	CONSTRAINT PK_IdTipoEvento PRIMARY KEY (IdTipoEvento)
);
GO

--Tabla cat�logo con las prioridades de un evento
CREATE TABLE [dbo].[PRIORIDAD]
(
	IdPrioridad		INT IDENTITY(1,1)	NOT NULL,
	Descripcion		NVARCHAR(30)		NOT NULL,
	CONSTRAINT PK_IdPrioridad PRIMARY KEY (IdPrioridad)
);
GO

--Tabla cat�logo con las clasificaciones de los cupones
CREATE TABLE [dbo].[CLASIFICACION_CUPON]
(
	IdClasificacionCupon	INT IDENTITY(1,1)	NOT NULL,
	Descripcion				NVARCHAR(30)		NOT NULL,
	CONSTRAINT PK_IdClasificacionCupon PRIMARY KEY(IdClasificacionCupon)
);
GO
/* TABLA OBSOLETA: En la interfaz, el usuario va a tener la opci�n de seleccionar a quienes van esos cupones, recibo la respuesta
	y mediante un store procedure ya verifico a quien le pega el cup�n: si es para estudiante, empleado, solo hombres, solo mujeres.
CREATE TABLE [dbo].[TIPO_CUPON](
	IdTipoCupon	  INT IDENTITY(1,1)	 NOT NULL,
	Descripcion	  NVARCHAR(30)		 NOT NULL,
	CONSTRAINT PK_IdTipoCupon PRIMARY KEY(IdTipoCupon)
);
GO

*/

--Tabla con los datos de los paises
CREATE TABLE [dbo].[PAIS]
(
	IdPais		INT IDENTITY(1,1)	NOT NULL,
	Descripcion NVARCHAR(40)		NOT NULL,
	CONSTRAINT PK_IdPais PRIMARY KEY (IdPais)
);
GO

--Tabla con los estados de la rep�blica mexicana
CREATE TABLE [dbo].[ESTADO]
(
	IdEstado	INT IDENTITY(1,1)	NOT NULL,
	IdPais		INT					NOT NULL,
	NomEstado	NVARCHAR(40)		NOT NULL,
	CONSTRAINT PK_IdEstado		PRIMARY KEY(IdEstado),
	CONSTRAINT FK_IdPais_Estado	FOREIGN KEY (IdPais) REFERENCES PAIS (IdPais)
);
GO

--Tabla de todos los municipios que hay en M�xico
CREATE TABLE [dbo].[MUNICIPIO]
(
	IdMunicipio		INT IDENTITY(1,1)	NOT NULL,
	IdEstado		INT					NOT NULL,
	NomMunicipio	NVARCHAR(80)		NOT NULL,
	CONSTRAINT PK_IdMunicipio			PRIMARY KEY (IdMunicipio),
	CONSTRAINT FK_IdEstado_Municipio	FOREIGN KEY (IdEstado)	REFERENCES ESTADO (IdEstado)
);
GO

--Tabla con los datos precargados de los empleados y alumnos de la universidad
CREATE TABLE [dbo].[POBLACION_UNIVERSITARIA]
(
	IdPoblacionUniversitaria	INT IDENTITY(1,1)	NOT NULL,
	IdRol						INT					NOT NULL,
	IdGenero					INT					NOT NULL,
	Nombre						NVARCHAR(30)		NOT NULL,
	ApellidoPat					NVARCHAR(25)		NOT NULL,
	ApellidoMat					NVARCHAR(25)		NOT NULL,
	NoIdentificador				NVARCHAR(25)		NOT NULL,
	FechaEgreso					DATE				NULL,
	CONSTRAINT PK_IdPoblacionUniversitaria		   PRIMARY KEY(IdPoblacionUniversitaria),
	CONSTRAINT FK_IdRol_PoblacionUniversitaria	   FOREIGN KEY (IdRol)					REFERENCES ROL (IdRol),
	CONSTRAINT FK_IdGenero_PoblacionUniversitaria	FOREIGN KEY (IdGenero)				REFERENCES GENERO (IdGenero),

);
GO

--Tabla con los usuarios dentro del sistema
CREATE TABLE [dbo].[USUARIO]
(
	IdUsuario					INT IDENTITY(1,1)	NOT NULL,
	IdRol						INT					NOT NULL,
	IdPoblacionUniversitaria	INT			     		NULL,
	IdEstatus					INT					NOT NULL,
	IdGenero					INT					NOT NULL,
	NombreUsuario				NVARCHAR(30)			NULL,
	Correo						NVARCHAR(40)			NULL,
	Contrase�a					NVARCHAR(30)		NOT NULL,
	NoIdentificador				NVARCHAR(25)			NULL,
	FechaRegistro				DATETIME			NOT NULL,
	FechaEgreso					DATE					NULL,
	CONSTRAINT PK_IdUsuario							PRIMARY KEY (IdUsuario),
	CONSTRAINT FK_IdEstatus_Usuario					FOREIGN KEY (IdEstatus)				    REFERENCES ESTATUS (IdEstatus),
	CONSTRAINT FK_IdRol_Usuario						FOREIGN KEY (IdRol)						REFERENCES ROL (IdRol),
	CONSTRAINT FK_IdPoblacionUniversitaria_Usuario  FOREIGN KEY (IdPoblacionUniversitaria)  REFERENCES POBLACION_UNIVERSITARIA (IdPoblacionUniversitaria),
	CONSTRAINT FK_IdGenero_Usuario Foreign key (IdGenero) REFERENCES GENERO (IdGenero)
);
GO

--Eventos del usuario
CREATE TABLE [dbo].[EVENTO]
(
	IdEvento		INT IDENTITY(1,1)	NOT NULL,
	IdUsuario		INT					NOT NULL,
	IdPrioridad		INT					NOT NULL,
	IdTipoEvento	INT					NOT NULL,
	Titulo			NVARCHAR(30)		NOT NULL,
	Descripcion		NVARCHAR(50)		NOT NULL,
	Notificacion	BIT					NOT NULL,
	FechaInicio		DATETIME			NOT NULL,
	FechaFin		DATETIME			NOT NULL,
	CONSTRAINT PK_IdEvento				PRIMARY KEY(IdEvento),
	CONSTRAINT FK_IdUsuario_Evento		FOREIGN KEY(IdUsuario)		REFERENCES USUARIO (IdUsuario),
	CONSTRAINT FK_IdPrioridad_Evento	FOREIGN KEY (IdPrioridad)	REFERENCES PRIORIDAD(IdPrioridad),
	CONSTRAINT FK_IdTipoEvento_Evento	FOREIGN KEY (IdTipoEvento)	REFERENCES TIPO_EVENTO(IdTipoEvento),
);
GO

--Cupones de los usuarios
CREATE TABLE [dbo].[CUPON]
(
	IdCupon					INT IDENTITY(1,1)	NOT NULL,
	IdClasificacionCupon	INT					NOT NULL,
	IdEstatus				INT					NOT NULL,
	Descripcion				NVARCHAR(50)		NOT NULL,
	Imagen					NVARCHAR(MAX)		NOT NULL,
	Restricciones			NVARCHAR(60)		NOT NULL,
	Codigo					NVARCHAR(30)		NOT NULL,
	FechaCreacion			DATETIME			NOT NULL,
	FechaAsignacion			DATETIME				NULL,
	FechaInicioVigencia		DATETIME			NOT NULL,
	FechaFinalVigencia		DATETIME			NOT NULL,
	CONSTRAINT PK_IdCupon	PRIMARY KEY(IdCupon),
	CONSTRAINT FK_IdClasificacionCupon_Cupon FOREIGN KEY (IdClasificacionCupon) REFERENCES CLASIFICACION_CUPON (IdClasificacionCupon),
	CONSTRAINT FK_IdEstatus_Cupon			 FOREIGN KEY (IdEstatus)			REFERENCES dbo.Estatus (IdEstatus)
);
GO

--Tabla del cup�n cambiado/canjeado del usuario
CREATE TABLE [dbo].[CUPON_CANJEADO]
(
	IdCuponCanjeado		INT IDENTITY(1,1)	NOT NULL,
	IdCupon				INT					NOT NULL,
	IdUsuario			INT					NOT NULL,
	FechaCanjeado		DATETIME				NULL,
	CONSTRAINT	PK_IdCuponCanjeado			PRIMARY KEY(IdCuponCanjeado),
	CONSTRAINT FK_IdCupon_CuponCanjeado		FOREIGN KEY (IdCupon)		REFERENCES CUPON (IdCupon),
	CONSTRAINT FK_IdUsuario_CuponCanjeado	FOREIGN KEY (IdUsuario)		REFERENCES USUARIO (IdUsuario)
);
GO

--Contactos del usuario
CREATE TABLE [dbo].[CONTACTO]
(
	IdContacto		INT IDENTITY(1,1)	NOT NULL,
	IdUsuario		INT					NOT NULL,
	IdTipoContacto	INT					NOT NULL,
	IdPais			INT					NOT NULL,
	IdMunicipio		INT						NULL,
	Estado			NVARCHAR(40)			NULL,
	Municipio		NVARCHAR(40)			NULL,
	Nombres			NVARCHAR(40)		NOT NULL,
	ApellidoPat		NVARCHAR(25)		NOT NULL,
	ApellidoMat		NVARCHAR(25)		NOT NULL,
	Colonia			NVARCHAR(30)		NOT NULL,
	Calle			NVARCHAR(30)		NOT NULL,
	NoInterior		INT						NULL,
	NoExterior		INT					    NULL,
	Foto			NVARCHAR(MAX)			NULL,
	FechaNac		DATE					NULL,
	FechaRegistro	DATETIME			NOT NULL
	CONSTRAINT PK_IdContacto				PRIMARY KEY (IdContacto),
	CONSTRAINT FK_IdMunicipio_Contacto	    FOREIGN KEY (IdMunicipio)	 REFERENCES MUNICIPIO (IdMunicipio),
	CONSTRAINT FK_IdUsuario_Contacto		FOREIGN KEY (IdUsuario)		 REFERENCES USUARIO (IdUsuario),
	CONSTRAINT FK_IdTipoContacto_Contacto   FOREIGN KEY (IdTipoContacto) REFERENCES TIPO_CONTACTO (IdTipoContacto),
	CONSTRAINT FK_IdPais_Contacto			FOREIGN KEY (IdPais)		 REFERENCES PAIS (IdPais),
);
GO

--Correos del usuario
CREATE TABLE [dbo].[CORREO]
(
	IdCorreo		INT IDENTITY(1,1)	NOT NULL,
	IdContacto		INT					NOT NULL,
	IdTipoCorreo	INT					NOT NULL,
	Email			NVARCHAR(35)		NOT NULL,
	CONSTRAINT PK_IdCorreo            PRIMARY KEY (IdCorreo),
	CONSTRAINT FK_IdContacto_Correo   FOREIGN KEY (IdContacto)      REFERENCES CONTACTO (IdContacto) ON DELETE CASCADE,
	CONSTRAINT FK_IdTipoCorreo_Correo FOREIGN KEY (IdTipoCorreo)    REFERENCES TIPO_CORREO (IdTipoCorreo),
);
GO

--Telefonos del usuario
CREATE TABLE [dbo].[TELEFONO]
(
	IdTelefono		INT IDENTITY(1,1)	NOT NULL,
	IdContacto		INT					NOT NULL,
	IdTipoTelefono	INT					NOT NULL,
	Numero			INT					NOT NULL,
	Extension		INT						NULL,
	CONSTRAINT PK_IdTelefono				PRIMARY KEY (IdTelefono),
	CONSTRAINT FK_IdContacto_Telefono		FOREIGN KEY(IdContacto)			REFERENCES CONTACTO (IdContacto)	ON DELETE CASCADE,
	CONSTRAINT FK_IdTipoTelefono_Telefono   FOREIGN KEY (IdTipoTelefono)    REFERENCES TIPO_TELEFONO (IdTipoTelefono),
);
GO
-------------------------------------------- TABLAS CAT�LOGO --------------------------------------------------------

INSERT INTO TIPO_TELEFONO
VALUES	('Fijo'),
		('M�vil'),
		('Oficina');
GO

INSERT INTO TIPO_EVENTO
VALUES	('Escolar'),
		('Personal'),
		('Laboral'),
		('Otro');
GO

INSERT INTO PRIORIDAD
VALUES	('Alta'),
		('Media'),
		('Baja');
GO

INSERT INTO TIPO_CONTACTO
VALUES	('Familiar'),
		('Trabajo'),
		('Amigo'),
		('Escolar');
GO

INSERT INTO TIPO_CORREO
VALUES	('Principal'),
		('Secundario');
GO

INSERT INTO GENERO
VALUES	('Masculino'), --G�nero masculino
		('Femenino'), --G�nero femenino
		('Sin especificar'); --Sin espeficicar
GO

INSERT INTO CLASIFICACION_CUPON
VALUES	('Alimentos'),
		('Entretenimiento'),
		('Ropa y calzado'),
		('Servicios'),
		('Acad�micos');
GO

INSERT INTO ESTATUS 
VALUES	('Activo'),
		('Inactivo'),
		('Vencido'),
		('Cancelado');
GO

INSERT INTO ROL
VALUES	('Administrador'),
		('Estudiante'),
		('Empleado');
GO
---------------------------------------------------------------------------------------------------------------------

-------------------------------------------- DATOS PRECARGADOS --------------------------------------------------------
INSERT INTO POBLACION_UNIVERSITARIA
VALUES	(3,1,'Ever Misael','Navarro','De La Cerda','AC4512',NULL),
		(3,1,'Jonathan Yair','Vazquez','Buenrostro','AC4589',NULL),
		(3,2,'Edith Eunice','Miranda','Rodriguez','AC9685',NULL),
		(3,1,'Raul Antonio','Rodriguez','Chavira','AC3625',NULL),
		(3,1,'Jesus Alejandro','Lopez','Garcia','AC0254',NULL),
		(2,1,'Antonio David','Tellez','Flores','1818402',CONVERT(DATE,'21-12-2022',103)),
		(2,1,'Jesus Alejandro','Lopez','Garcia','1739259',CONVERT(DATE,'12-17-2021',101)),
		(2,2,'Edith Eunice','Miranda','Rodriguez','1521662',CONVERT(DATE,'12-15-2021',101)),
		(2,1,'Luis Arturo','Gonzalez','Morales','1845341',CONVERT(DATE,'21-12-2022',103)),
		(2,2,'Irma Mayra','Martinez','Garza','1833648',CONVERT(DATE,'21-12-2022',103)),
		(2,1,'Angel Arnulfo','Rodriguez','Hernandez','1677532',CONVERT(DATE,'21-12-2022',103)),
		(2,1,'Jose Alvaro','Rodriguez','Hernandez','1813148',CONVERT(DATE,'21-12-2022',103)),
		(2,1,'Jose Raul','Martinez','Carranco','1743517',CONVERT(DATE,'21-12-2022',103)),
		(2,1,'Raul Antonio','Rodriguez','Chavira','1864613',CONVERT(DATE,'21-12-2022',103)),
		(2,1,'Jonathan Yair','Vazquez','Buenrostro','1753521',CONVERT(DATE,'21-12-2022',103)),
		(2,2,'Ana Priscila','Cortez','Gutierrez','1869713',CONVERT(DATE,'21-12-2022',103)),
		(2,1,'Ever Misael','Navarro','De la cerda','1725196',CONVERT(DATE,'21-12-2022',103)),
		(2,1,'Bryan','Aguirre','Tudon','1735839',CONVERT(DATE,'21-12-2022',103)),
		(2,1,'Eduardo Guadalupe','M�ndez','Miranda','1807854',CONVERT(DATE,'21-12-2022',103)),
		(2,1,'Jes�s Valmiki','Perez','Cervantes','2083983',CONVERT(DATE,'21-12-2022',103));
GO

	
-- SP USUARIO
CREATE OR ALTER PROCEDURE [dbo].[spUSUARIO]
(
	@Accion				INT,
	@NoIdentificador	NVARCHAR(25)	=NULL,
	@IdGenero			INT				=NULL,
	@Email				NVARCHAR(40)	=NULL,
	@Contrase�a			NVARCHAR(30)	=NULL,
	@NombreUsuario		NVARCHAR(30)	=NULL,
	@Correo				NVARCHAR(30)	=NULL,
	@InicioSesion		NVARCHAR(40)	=NULL
)
AS
BEGIN
	--Dos acciones
	--Buscar Usuario Datos pre cargados y que no tengas una cuenta
	IF @Accion = 1
	BEGIN
	--BUSCAR SI ESTA EN MI DATOS PRECARGADOS
		IF EXISTS (SELECT 1 FROM POBLACION_UNIVERSITARIA WHERE NoIdentificador = @NoIdentificador) -- Nos muestra un 1 si hay un un valor en Datos Uusario
		BEGIN
		--BUSCAR SI NOTIENES UNA CUENTA
			IF NOT EXISTS (SELECT 1 FROM USUARIO WHERE NoIdentificador = @NoIdentificador )
			BEGIN
				SELECT * 
				FROM POBLACION_UNIVERSITARIA 
				WHERE NoIdentificador = @NoIdentificador
			END
		END
	END
		--Acci�n 2 Registrar un usuario
	IF @Accion=2
		BEGIN
		--Copiar datos precargados
		DECLARE @v_IdRol					INT,
				@v_IdPoblacionUniversitaria INT,
				@v_FechaEgreso				DATE

			/*SET @v_IdRol=(SELECT IdRol	
						  FROM POBLACION_UNIVERSITARIA
						  WHERE NoIdentificador=@NoIdentificador)*/

		SELECT	@v_IdRol=IdRol,
				@v_IdPoblacionUniversitaria= IdPoblacionUniversitaria,
				@v_FechaEgreso=FechaEgreso
		FROM POBLACION_UNIVERSITARIA
		WHERE NoIdentificador=@NoIdentificador

		--Validar si existe o no el correo con la cuenta
		IF NOT EXISTS (SELECT 1 FROM USUARIO WHERE Correo = @Email)
		BEGIN
		--Insert registrando el usuario si es que existe el correo
			INSERT INTO USUARIO (IdEstatus,IdRol,IdPoblacionUniversitaria,IdGenero,Correo, Contrase�a,NoIdentificador,FechaEgreso, FechaRegistro)
			VALUES (1,@v_IdRol,@v_IdPoblacionUniversitaria,@IdGenero,@Email,@Contrase�a,@NoIdentificador,@v_FechaEgreso,GETDATE())
		END
	END
	--Accion 3: Iniciar sesi�n, nos trae la info del usuario con el que nos vamos a logear.
	IF @Accion=3
	BEGIN
		SELECT *
		FROM USUARIO
		WHERE (Correo=@InicioSesion OR NombreUsuario=@InicioSesion) AND 
				Contrase�a=@Contrase�a
	END;
	--Accion 4: Traerme el nombre completo de un usuario por su id.
	IF @Accion=4
	BEGIN
		SELECT Nombre+' '+ApellidoPat+' '+ApellidoMat AS NombreCompleto
		FROM POBLACION_UNIVERSITARIA
		WHERE NoIdentificador=@NoIdentificador;
	END;
END;
GO

--SP Del contacto
CREATE OR ALTER PROCEDURE [dbo].[spCONTACTO]
(
	@Accion			INT,
	@IdContacto		INT				=NULL,
	@IdTipoContacto	INT				=NULL,
	@IdUsuario		INT				=NULL,
	@IdEstado		INT				=NULL,
	@IdMunicipio	INT				=NULL,
	@IdPais			INT				=NULL,
	@Estado			NVARCHAR(40)	=NULL,
	@Municipio		NVARCHAR(40)	=NULL,
	@Nombres		NVARCHAR(40)	=NULL,
	@ApellidoPat	NVARCHAR(25)	=NULL,
	@ApellidoMat	NVARCHAR(25)	=NULL,
	@Colonia		NVARCHAR(30)	=NULL,
	@Calle			NVARCHAR(30)	=NULL,
	@NoInterior		INT				=NULL,
	@NoExterior		INT				=NULL,
	@Foto			NVARCHAR(MAX)	=NULL,
	@FechaNac		DATE			=NULL,
	@Busqueda		NVARCHAR(50)	=NULL
)
AS
BEGIN
	--Accion 1: Registrar un contacto nuevo. Par�metros: Todos excepto Busqueda
	IF @Accion=1
	BEGIN
		INSERT INTO CONTACTO
		VALUES (@IdUsuario,@IdTipoContacto,@IdPais,@IdMunicipio,@Estado,@Municipio,@Nombres,@ApellidoPat,@ApellidoMat,@Colonia,@Calle,@NoInterior,@NoExterior,@Foto,@FechaNac,GETDATE())
		SELECT TOP 1 IdContacto 
		FROM CONTACTO 
		WHERE IdUsuario=@IdUsuario
		ORDER BY IdContacto DESC
	END;
	--Accion 2: Modificar un contacto. Par�metros: Todos excepto Busqueda
	IF @Accion=2
	BEGIN
		UPDATE CONTACTO
		SET	IdMunicipio=@IdMunicipio,
			IdTipoContacto=@IdTipoContacto,
			IdPais=@IdPais,
			Estado=@Estado,
			Municipio=@Municipio,
			Nombres=@Nombres,
			ApellidoPat=@ApellidoPat,
			ApellidoMat=@ApellidoMat,
			Colonia=@Colonia,
			Calle=@Calle,
			NoInterior=@NoInterior,
			NoExterior=@NoExterior,
			Foto=@Foto,
			FechaNac=@FechaNac
		WHERE IdContacto=@IdContacto AND IdUsuario=@IdUsuario
	END;
	--Accion 3: Borrar un contacto. Par�metros: IdContacto, IdUsuario
	IF @Accion=3
	BEGIN
		DELETE FROM CONTACTO
		WHERE IdContacto=@IdContacto AND IdUsuario=@IdUsuario
	END;
	--Accion 4: Obtener la info. de todos los contactos relacionados de un usuario. Par�metros: IdUsuario
	IF @Accion=4
	BEGIN
		SELECT * 
		FROM CONTACTO
		WHERE IdUsuario=@IdUsuario;
	END;
	--Accion 5: Desplegar la info. b�sica de los contactos de un usuario. Par�metros: IdUsuario
	IF @Accion=5
	BEGIN
		SELECT CON.IdContacto,
				CON.Foto,
			   (CON.Nombres+' '+CON.ApellidoPat+' '+CON.ApellidoMat) AS NombreCompleto,
			   TIC.Descripcion as TipoContacto
		FROM CONTACTO AS CON
			 INNER JOIN TIPO_CONTACTO AS TIC ON CON.IdTipoContacto=TIC.IdTipoContacto 
		WHERE IdUsuario=@IdUsuario 
	END;
	--Accion 6: Desplegar toda la info. de un contacto. Par�metros: IdContacto
	IF @Accion=6
	BEGIN
		SELECT CON.Nombres,
				CON.ApellidoPat,
				CON.ApellidoMat,
				CON.IdPais,
				CON.IdMunicipio,
				PAI.Descripcion AS NomPais,
				EST.NomEstado AS NomEstado,
				MUN.NomMunicipio AS NomMunicipio,
				CON.Estado,
				CON.Municipio,
				CON.Colonia,
				CON.Calle,
				CON.NoInterior, 
				CON.NoExterior,
				CON.FechaNac
		FROM CONTACTO AS CON
			LEFT JOIN MUNICIPIO AS MUN ON CON.IdMunicipio=MUN.IdMunicipio
			LEFT JOIN ESTADO AS EST ON MUN.IdEstado=EST.IdEstado
			LEFT JOIN PAIS AS PAI ON EST.IdPais=PAI.IdPais
			INNER JOIN TIPO_CONTACTO AS TIC ON CON.IdTipoContacto=TIC.IdTipoContacto
		WHERE IdContacto=@IdContacto AND IdUsuario=@IdUsuario
	END;
	--Accion 7: Buscar un contacto por su nombre y desplegar su info. b�sica. Par�metros: IdUsuario y Busqueda
	IF  @Accion=7
	BEGIN
		SELECT CON.Foto,
			   (CON.Nombres+' '+CON.ApellidoPat+' '+CON.ApellidoMat) AS NombreCompleto,
			   TIC.Descripcion
		FROM CONTACTO AS CON
			 INNER JOIN TIPO_CONTACTO AS TIC ON CON.IdTipoContacto=TIC.IdTipoContacto 
		WHERE IdUsuario=@IdUsuario AND 
				(Nombres LIKE '%'+@Busqueda+'%' OR
				ApellidoPat LIKE '%'+@Busqueda+'%' OR
				ApellidoMat LIKE '%'+@Busqueda+'%') 
	END
END;
GO

--SP de los correos
CREATE OR ALTER PROCEDURE [dbo].[spCORREO]
(
	@Accion			INT,
	@IdCorreo		INT				=NULL,
	@IdContacto		INT				=NULL,
	@IdTipoCorreo	INT				=NULL,
	@Email			NVARCHAR(35)	=NULL
)
AS
BEGIN
	--Accion 1: Insertar un correo nuevo de un contacto. Par�metros: IdContacto,IdTipoCorreo,Email
	IF @Accion=1
	BEGIN
		INSERT INTO CORREO
		VALUES (@IdContacto,@IdTipoCorreo,@Email)
	END
	--Accion 2: Obtener los id de los correos de un contacto. Par�metros: IdContacto
	IF @Accion=2
	BEGIN
		SELECT IdCorreo
		FROM CORREO
		WHERE IdContacto=@IdContacto
	END
	--Accion 3: Desplegar la info de los correos del contacto. Par�metros: IdContacto
	IF @Accion=3
	BEGIN
		SELECT IdCorreo,
				Email,
				TIC.Descripcion AS TipoCorreo
		FROM CORREO AS COR
			  INNER JOIN TIPO_CORREO AS TIC ON COR.IdTipoCorreo=TIC.IdTipoCorreo
		WHERE IdContacto=@IdContacto
	END
	--Accion 4: Editar un correo de un contacto. Par�mteros: IdTipoCorreo, Email, IdCorreo
	IF @Accion=4
	BEGIN
		UPDATE CORREO
		SET	IdTipoCorreo = @IdTipoCorreo,
			Email = @Email
		WHERE IdCorreo= @IdCorreo
	END
	--Acci�n 5: Eliminar un correo de un contacto. Par�metros: IdCorreo
	IF @Accion=5
	BEGIN
		DELETE FROM CORREO
		WHERE IdCorreo=@IdCorreo
	END
	--Acci�n 6: Eliminar todos los correos de un contacto. Par�metos: IdContacto
	IF @Accion=6
	BEGIN
		DELETE FROM CORREO
		WHERE IdContacto=@IdContacto
	END
END;
GO 

CREATE OR ALTER PROCEDURE [dbo].[spTELEFONO]
(
	@Accion			INT,
	@IdTelefono		INT		=NULL,
	@IdContacto		INT		=NULL,
	@IdTipoTelefono	INT		=NULL,
	@Numero			INT		=NULL,
	@Extension		INT		=NULL
)
AS
BEGIN
	--Acci�n 1: Agregar un nuevo tel�fono. Par�metros: IdContacto, IdTipoTelefono, Numero, Extension
	IF @Accion=1
	BEGIN
		INSERT INTO TELEFONO
		VALUES (@IdContacto,@IdTipoTelefono,@Numero,@Extension)
	END
	--Accion 2: Obtener los id de los telefonos de un contacto. Par�metros: IdContacto
	IF @Accion=2
	BEGIN
		SELECT *
		FROM TELEFONO
		WHERE IdContacto=@IdContacto
	END
	--Accion 3: Desplegar la info de los telefonos de un contacto. Par�metros: IdContacto
	IF @Accion=3
	BEGIN
		SELECT IdTelefono,
				Numero,
				Extension,
				TIT.Descripcion AS TipoTelefono
		FROM TELEFONO AS TEL
			  INNER JOIN TIPO_TELEFONO AS TIT ON TEL.IdTipoTelefono=TIT.IdTipoTelefono
		WHERE IdContacto=@IdContacto
	END
	--Accion 4: Editar un numero de un contacto. Par�mteros: IdTipoTelefono, Numero, Extension,IdCorreo
	IF @Accion=4
	BEGIN
		UPDATE TELEFONO
		SET	IdTipoTelefono = @IdTipoTelefono,
			Extension=@Extension,
			Numero=@Numero
		WHERE IdTelefono= @IdTelefono
	END
	--Acci�n 5: Eliminar un telefono de un contacto. Par�metros: IdTelefono
	IF @Accion=5
	BEGIN
		DELETE FROM TELEFONO
		WHERE IdTelefono=@IdTelefono
	END
	--Acci�n 6: Eliminar todos los telefonos de un contacto. Par�metos: IdContacto
	IF @Accion=6
	BEGIN
		DELETE FROM TELEFONO
		WHERE IdContacto=@IdContacto
	END
END;
GO


/*
exec spCONTACTO @Accion=1,
				@IdMunicipio=1,
				@IdUsuario=4,
				@IdTipoContacto=2,
				@IdPais=4,
				@Nombres='Benito',
				@ApellidoPat='Camela',
				@ApellidoMat='Todos los viernes',
				@Colonia='Del valle',
				@Calle='Pedregal',
				@NoExterior=4243
				
*/


--Store de los combos
CREATE OR ALTER PROCEDURE [dbo].[spCOMBOS]
(
	@Accion		INT,
	@IdPais		INT	=NULL,
	@IdEstado	INT	=NULL
)
AS
BEGIN
	--Acci�n 1: Combo del g�nero
	IF @Accion=1
	BEGIN
		SELECT *
		FROM GENERO
		ORDER BY Descripcion ASC
	END;
	--Acci�n 2: Combo del tipo de cup�n
	IF @Accion=2
	BEGIN
		SELECT *
		FROM CLASIFICACION_CUPON
		ORDER BY Descripcion ASC
	END;
	--Acci�n 3: Combo del tipo de contacto
	IF @Accion=3
	BEGIN
			SELECT *
			FROM TIPO_CONTACTO
			ORDER BY Descripcion ASC
	END;
	--Acci�n 4: Combo del tipo de correo
	IF @Accion=4
	BEGIN
		SELECT *
		FROM TIPO_CORREO
		ORDER BY Descripcion ASC
	END;
	IF @Accion=5
	BEGIN
		SELECT *
		FROM TIPO_TELEFONO	
		ORDER BY Descripcion ASC
	END;
	IF @Accion=6
	BEGIN
		SELECT * 
		FROM TIPO_EVENTO
		ORDER BY Descripcion ASC
	END;
	IF @Accion=7
	BEGIN
		SELECT *
		FROM PRIORIDAD
		ORDER BY Descripcion ASC
	END;
	IF @Accion=8
	BEGIN
		SELECT *
		FROM PAIS 
		ORDER BY Descripcion ASC
	END;
	IF @Accion=9
	BEGIN
		SELECT *
		FROM ESTADO
		WHERE IdPais=@IdPais
		ORDER BY NomEstado ASC
	END;
	IF @Accion=10
	BEGIN
		SELECT *
		FROM MUNICIPIO
		WHERE IdEstado=@IdEstado
		ORDER BY NomMunicipio ASC
	END;
	IF @Accion=11
	BEGIN
		SELECT *
		FROM ESTATUS
		ORDER BY Descripcion ASC
	END;
	IF @Accion=12
	BEGIN
		SELECT *
		FROM ROL
		ORDER BY Descripcion ASC
	END;
END;
GO


CREATE OR ALTER PROCEDURE [dbo].[spEVENTO]
(
	@Accion			INT,
	@IdEvento		INT				=NULL,
	@IdUsuario		INT				=NULL,
	@IdPrioridad	INT				=NULL,
	@IdTipoEvento	INT				=NULL,
	@Titulo			NVARCHAR(30)	=NULL,
	@Descripcion	NVARCHAR(50)	=NULL,
	@Notificacion	BIT				=NULL,
	@FechaInicio	DATETIME		=NULL,
	@FechaFin		DATETIME		=NULL,
	@Busqueda		NVARCHAR(50)	=NULL,
	@Ordenar		INT				=NULL --1: Ordenar por prioridad, 2: Ordenar por pr�ximos a iniciar
)
AS
BEGIN
	--Acci�n 1: Crear un evento nuevo. Par�metros: Todos excepto Ordenar y Busqueda
	IF @Accion=1
	BEGIN
		INSERT INTO EVENTO 
		VALUES	(@IdUsuario,@IdPrioridad,@IdTipoEvento,@Titulo,@Descripcion,@Notificacion,CONVERT(DATE,@FechaInicio,101),CONVERT(Date,@FechaFin,101))
	END;
	--Acci�n 2: Traer el id de cada uno de los eventos relacionados a un usuario. Par�metros: IdUsuario
	IF @Accion=2
	BEGIN
		SELECT IdEvento
		FROM EVENTO
		WHERE IdUsuario=@IdUsuario
	END;
	--Acci�n 3: Busca un evento de un usuario y puede ser filtrado  por su T�tulo o Descripcion. Par�metros: IdUsuario, Busqueda, Ordenar
	IF @Accion=3
	BEGIN
		IF @Ordenar IS NULL --Por si no queremos ordenarlo
		BEGIN
			SELECT EVE.IdEvento,
					PRI.Descripcion AS Prioridad,
					TIE.Descripcion AS TipoEvento,
					EVE.Titulo,
					EVE.Descripcion,
					EVE.FechaFin,
					EVE.FechaInicio,
					EVE.Notificacion
			FROM EVENTO AS EVE
				INNER JOIN PRIORIDAD AS PRI ON EVE.IdPrioridad=PRI.IdPrioridad
				INNER JOIN TIPO_EVENTO AS TIE ON EVE.IdTipoEvento=TIE.IdTipoEvento
			WHERE (IdUsuario=@IdUsuario OR IdUsuario IS NULL) AND
					(((EVE.Titulo LIKE '%'+@Busqueda+'%' OR @Busqueda IS NULL) OR (EVE.Descripcion LIKE '%' +@Busqueda+'%' OR @Busqueda IS NULL)) AND  IdUsuario=@IdUsuario)
			ORDER BY CASE WHEN @Ordenar IS NULL THEN EVE.IdEvento END ASC,
					CASE WHEN @Ordenar=1 THEN EVE.IdPrioridad END ASC,
					CASE WHEN @Ordenar=2 THEN EVE.FechaInicio END ASC
		END;
	END;
	--Acci�n 4: Trae la informaci�n de un evento. Par�metros: IdUsuario, IdEvento
	IF @Accion=4
	BEGIN
		SELECT	IdPrioridad,
				IdTipoEvento,
				Titulo,
				Descripcion,
				FechaFin,
				FechaInicio,
				Notificacion
		FROM EVENTO
		WHERE IdEvento=@IdEvento AND IdUsuario=@IdUsuario
	END;
	--Acci�n 5: Edita un evento por su ID. Par�metros: IdEvento, Titulo, Descripcion, IdTipoEvento, IdpRIORIDAD, FechaInicio, FechaFin, Notificacion, IdUsuario
	IF @Accion=5 
	BEGIN
		UPDATE EVENTO
		SET Titulo=@Titulo,
			Descripcion=@Descripcion,
			IdTipoEvento=@IdTipoEvento,
			IdPrioridad=@IdPrioridad,
			FechaInicio=@FechaInicio,
			FechaFin=@FechaFin,
			Notificacion=@Notificacion
		WHERE IdEvento=@IdEvento AND IdUsuario=@IdUsuario
	END;
	--@Acci�n 6: Elimina un evento por su ID. Par�metros: IdEvento
	IF @Accion=6
	BEGIN
		DELETE FROM EVENTO
		WHERE IdEvento=@IdEvento
	END;
END;
GO


CREATE OR ALTER PROCEDURE [dbo].[spINFORMACION]
(
	@Accion	INT,
	@NoIdentificador	NVARCHAR(25)		=NULL,
	@IdPoblacion		INT					=NULL,
	@NombreCompleto		NVARCHAR(100)		=NULL,
	@IdRol				INT					=NULL,
	@IdGenero			INT					=NULL,
	@Nombre				NVARCHAR(30)		=NULL,
	@ApellidoPat		NVARCHAR(25)		=NULL,
	@ApellidoMat		NVARCHAR(25)		=NULL,
	@FechaEgreso		DATE				=NULL
)
AS
BEGIN
	--Acci�n 1: Verificar si ya existe un registro. Par�metros: NoIdentificador, NombreCompleto, IdRol
	--CASOS: Si yo voy a meter a alguien con una matr�cula/clave SIASE que ya existe, me debe de traer un resultado
	--			Si quiero meter a alguien, debo verificar que NO SEA EL MISMO NOMBRE, pero tambi�n, debo de checar si tienen el mismo IdRol, por que yo puedo tener a 2 usuarios en la base
	--			con el mismo nombre, pero con un rol diferente ya que un alumno tambi�n puede ser un empleado
	IF @Accion=1	
	BEGIN
			SELECT *
			FROM POBLACION_UNIVERSITARIA
			WHERE (NoIdentificador=@NoIdentificador) OR
				  ((Nombre+' '+ApellidoPat+' '+ApellidoMat LIKE '%'+@NombreCompleto+'%') AND IdRol=@IdRol)

	END;
	--Acci�n 2: Ingresar un nuevo registro a la base de datos. Par�metros: IdRol, IdGenero, Nombre, ApellidoPat, ApellidoMat, NoIdentificador, FechaEgreso
	IF @Accion=2
	BEGIN
		INSERT INTO POBLACION_UNIVERSITARIA
		VALUES	(@IdRol,@IdGenero,@Nombre,@ApellidoPat, @ApellidoMat,@NoIdentificador,@FechaEgreso)
	END;
	--Acci�n 3: Editar un registro por su id. Par�metros: IdPoblacion, IdRol, NoIdentificador, Nombre, ApellidoPat, ApellidoMat, FechaEgreso
	IF @Accion=3
	BEGIN
		UPDATE POBLACION_UNIVERSITARIA
		SET IdRol=@IdRol,
			NoIdentificador=@NoIdentificador,
			Nombre=@Nombre,
			ApellidoPat=@ApellidoPat,
			ApellidoMat=@ApellidoMat,
			FechaEgreso=@FechaEgreso
		WHERE IdPoblacionUniversitaria=@IdPoblacion
	END;
	--Accion 4: Borrar un registro por su id. Par�metros: IdPoblacion
	IF @Accion=4
	BEGIN
		--Primero modificamos la informaci�n de la tabla usuario: Borro la relaci�n de la llave foranea y le cambio su estatus a inactivo.
			UPDATE USUARIO
			SET IdEstatus=2,
				IdPoblacionUniversitaria=NULL
			WHERE IdPoblacionUniversitaria=@IdPoblacion
		--Luego borro la informaci�n de la tabla de info. precargada
			DELETE FROM POBLACION_UNIVERSITARIA
			WHERE IdPoblacionUniversitaria=@IdPoblacion
	END;
	--Accion 5: Traerme toda la info. de los usuarios precargados, podemos filtrala con una b�squeda. Par�metros: IdPoblacion, NombreCompleto
	IF @Accion=5
	BEGIN
		SELECT *
		FROM POBLACION_UNIVERSITARIA
		WHERE (IdPoblacionUniversitaria=@IdPoblacion OR @IdPoblacion IS NULL) AND
				(Nombre+' '+ApellidoPat+' '+ApellidoMat LIKE '%'+@NombreCompleto+'%' OR @NombreCompleto IS NULL)
	END;
END;
GO


--SP de los cupones

CREATE OR ALTER PROCEDURE [dbo].[spCUPON]
(
	@Accion					INT,
	@IdCupon				INT		=NULL,
	@IdClasificacionCupon	INT		=NULL,
	@IdEstatus				INT		=NULL,
	@Descripcion			NVARCHAR(50)	=NULL,
	@Imagen					NVARCHAR(MAX)	=NULL,
	@Restricciones			NVARCHAR(60)	=NULL,
	@Codigo					NVARCHAR(30)	=NULL,
	@FechaCreacion			DATETIME		=NULL,
	@FechaAsignacion		DATETIME		=NULL,
	@FechaInicioVigencia	DATETIME		=NULL,
	@FechaFinalVigencia		DATETIME		=NULL,
	@Busqueda				NVARCHAR(60)   =NULL,
	@IdUsuario				INT				=NULL,
	@IdCuponCanjeado		INT				=NULL,
	@Combinacion			INT				=NULL
)
AS
BEGIN

	--Acci�n 1: Dar de alta un nuevo cup�n. Par�metros: Todos excepto IdCupon y Busqueda
	IF @Accion=1
	BEGIN
	   INSERT INTO CUPON
           VALUES (@IdClasificacionCupon,1,@Descripcion,@Imagen,@Restricciones,@Codigo,@FechaCreacion,@FechaAsignacion,@FechaInicioVigencia,@FechaFinalVigencia)
	END;
       --Acci�n 2: Traerme toda la info de los cupones disponibles, la podemos filtrar por la descripci�n y/o clasificacion, esta acci�n me sirve tanto para el admin como los usuarios. Par�metros: IdClasificacionCupon, Busqueda
       IF @Accion=2
       BEGIN
           SELECT *
           FROM CUPON
           WHERE (Descripcion LIKE '%'+ @Busqueda + '%' OR @Busqueda IS NULL) AND
                 (IdClasificacionCupon=@IdClasificacionCupon OR @IdClasificacionCupon IS NULL) AND
				 IdEstatus=1
       END;
       --Acci�n 3: Modificar los detalles de un cup�n. Par�metros: IdClasificacionCupon, Descripcion, Imagen, Restricciones, FechaInicioVigencia, FechaFinalVigencia, IdCupon
       IF @Accion=3
       BEGIN
       UPDATE CUPON
       SET IdClasificacionCupon=@IdClasificacionCupon,
           Descripcion=@Descripcion,
           Imagen=@Imagen,
           Restricciones=@Restricciones,
           FechaInicioVigencia=@FechaInicioVigencia,
           FechaFinalVigencia=@FechaFinalVigencia
      WHERE IdCupon=@IdCupon
     END
	 --Acci�n 4: Dar de baja l�gica un cup�n por su Id. Par�metros: IdCupon
	 IF @Accion=4
	 BEGIN
	 	 UPDATE CUPON
		 SET IdEstatus=4
		 WHERE IdCupon=@IdCupon;
		END;
	 --Acci�n 5: Mostrar los cupones DISPONIBLES para que el usuario pueda canjear. P�rametros: IdUsuario
	IF @Accion=5
	BEGIN
		SELECT * 
		FROM CUPON AS CUP
			INNER JOIN CUPON_CANJEADO AS CUC ON CUP.IdCupon=CUC.IdCupon
		WHERE (CUC.IdUsuario=@IdUsuario) 
			  AND (CUP.FechaInicioVigencia <= GETDATE() AND  CUP.FechaFinalVigencia >=GETDATE())  --Preguntamos si la fecha de inicio es menor o igual a la fecha de hoy y si la fecha de vigencia es mayor o igual para ver si se puede canjear
			  AND CUC.FechaCanjeado IS NULL --Le decimos que nos traiga aquellos cupones que aun no han sido canjeados
	END;
	--Acci�n 6: Canjear un cup�n por su ID. Par�metro: IdCuponCanjeado
	IF @Accion=6
	BEGIN
		UPDATE CUPON_CANJEADO
		SET FechaCanjeado=GETDATE()
		WHERE IdCuponCanjeado=@IdCuponCanjeado
	END;
	--Acci�n 7: Obtener el id de las personas a las que se les va a asignar un cup�n por su rol/g�nero. Par�metros: Combinaci�n
	IF @Accion=7
	BEGIN
	--2: Estudiante  3:Empleado   1: Masculino 2: Femenino 3:n/a
		--Primera combinaci�n: Solo estudiantes masculinos
		IF @Combinacion=1
		BEGIN
			SELECT IdUsuario 
			FROM USUARIO
			WHERE IdRol=2 AND IdGenero=1 AND IdEstatus=1
		END;
		--Segunda combinaci�n: Solo estudiantes femeninos
		IF @Combinacion=2
		BEGIN
			SELECT IdUsuario 
			FROM USUARIO
			WHERE IdRol=2 AND IdGenero=2 AND IdEstatus=1
		END;
		--Tercera combinaci�n: Todos los estudiantes
		IF @Combinacion=3
		BEGIN
			SELECT IdUsuario 
			FROM USUARIO
			WHERE IdRol=2 AND IdEstatus=1
		END;
		--Cuarta combinaci�n: Solo empleados masculinos
		IF @Combinacion=4
		BEGIN
			SELECT IdUsuario 
			FROM USUARIO
			WHERE IdRol=3 AND IdGenero=1 AND IdEstatus=1
		END;
		--Quinta combinaci�n: Solo empleados femeninos
		IF @Combinacion=5
		BEGIN
			SELECT IdUsuario 
			FROM USUARIO
			WHERE IdRol=3 AND IdGenero=2 AND IdEstatus=1
		END;
		--Sexta combinaci�n: Todos los empleados
		IF @Combinacion=6
		BEGIN
			SELECT IdUsuario 
			FROM USUARIO
			WHERE IdRol=3 AND IdEstatus=1
		END;
		--S�ptima combinaci�n: Todos los varones
		IF @Combinacion=7
		BEGIN
			SELECT IdUsuario 
			FROM USUARIO
			WHERE IdGenero=2 AND IdEstatus=1
		END;
		--Octava combinaci�n: Todas las mujeres
		IF @Combinacion=8
		BEGIN
			SELECT IdUsuario 
			FROM USUARIO
			WHERE IdGenero=3 AND IdEstatus=1
		END;
		--Novena combinaci�n: Todos en la poblaci�n universitaria
		IF @Combinacion=9
		BEGIN
			SELECT IdUsuario 
			FROM USUARIO
			WHERE IdEstatus=1
		END;
		
	END;
	--Acci�n 8: Asignar una instancia de un cup�n a un conjunto de usuarios. Par�metros: IdUsuario, IdCupon
	IF @Accion=8
	BEGIN
		INSERT INTO CUPON_CANJEADO (IdCupon,IdUsuario)
		VALUES (@IdCupon,@IdUsuario)
	END;
END;
GO


exec spCONTACTO @Accion=5,@IdUsuario=1

select * from CORREO
select * from telefono