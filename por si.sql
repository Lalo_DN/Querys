USE DBSIGEF

GO

---Consulta exigente

SELECT SUM(BOL.Monto) AS 'Monto Pagado',
		(SELECT IdUsuario FROM USUARIO AS USU WHERE USU.IdUsuario=BOL.IdUsuario) AS 'Folio',
		NombreCompleto=CONCAT((SELECT Nombre FROM USUARIO AS USU WHERE USU.IdUsuario=BOL.IdUsuario),' ',(SELECT Apellidos FROM USUARIO AS USU WHERE USU.IdUsuario=BOL.IdUsuario))
FROM BOLETA AS BOL
WHERE BOL.IdEstatus=(SELECT IdEstatus
					  FROM ESTATUS AS EST
					   WHERE EST.Descrip='Approved') AND
	 BOL.FechaRevision BETWEEN '1/10/2021' AND '30/11/2021'
GROUP BY BOL.IdUsuario
HAVING SUM(BOL.Monto)>=250
ORDER BY NombreCompleto

--Muestra el idusuario y nombre completo de los usuarios que subieron la mayor cantidad de ponencias
SELECT IdUsuario ,
		NombreCompleto=CONCAT((SELECT Nombre FROM USUARIO AS USU WHERE USU.IdUsuario=PON.IdUsuario),' ',(SELECT Apellidos FROM USUARIO AS USU WHERE USU.IdUsuario=PON.IdUsuario)) 
FROM PONENCIA AS PON
WHERE IdUsuario IN (SELECT  IdUsuario
					FROM PONENCIA
					GROUP BY IduSUARIO
					HAVING COUNT(IdPonencia)=(SELECT  MAX(totalPon)
											  FROM ( SELECT  IdUsuario,
											  COUNT(IdPonencia) AS totalPon
											  FROM PONENCIA  
											 GROUP BY IdUsuario) AS s))
GROUP BY IdUsuario


SELECT  IdUsuario
FROM PONENCIA
GROUP BY IduSUARIO
HAVING COUNT(IdPonencia)=(SELECT  MAX(totalPon)
					  FROM ( SELECT  IdUsuario,
					  count(IdPonencia) AS totalPon
					  from PONENCIA  
						group by IdUsuario) as s)
