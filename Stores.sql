USE DBDULCERIA;
GO 

--Un SP siempre se queda en la base de datos cu�ndo lo creo.
--EXEC spNombre [@Par�metro1,@Par�metro2,...]

--[CREATE OR ALTER]
CREATE PROCEDURE spNombreSP --Siempre empieza con "sp" en min�scula
(
	--[@Parametros]
)
AS
BEGIN
	
END
GO 

ALTER PROCEDURE [dbo].[spCategoria]
(
	@Accion			INT,
	@IdTipoProducto INT				= NULL,
	@Descripcion	NVARCHAR(80)	= NULL,
	@IdCategoria	INT				= NULL
)
AS
BEGIN
--Consulta categor�a
	IF @Accion=1
	BEGIN
		SELECT	CAT.IdCategoria,
				TIP.Descripcion AS TipoProducto,
				CAT.Descripcion AS Categoria
		FROM CATEGORIA AS CAT
			LEFT JOIN TIPO_PRODUCTO AS TIP ON CAT.IdTipoProducto=TIP.IdTipoProducto
	END;
	--Registra categorias
	IF @Accion=2
	BEGIN
		INSERT INTO CATEGORIA (IdTipoProducto,Descripcion)
		VALUES (@IdTipoProducto,@Descripcion)
	END;
	--Actualizar la categor�a
	IF @Accion=3
	BEGIN
		UPDATE CATEGORIA
		SET Descripcion=@Descripcion
		WHERE IdCategoria=@IdCategoria
	END;
	--Borrar registro por categor�a
	IF @Accion=4
	BEGIN
		DELETE CATEGORIA
		WHERE IdCategoria=@IdCategoria
	END;
END;
GO

EXEC spCategoria @Accion=2,
				 @IdTipoProducto=2,
				 @Descripcion='Paletas Vero';
GO

EXEC spCategoria @Accion=1,
				 @IdTipoProducto=NULL,
				 @Descripcion=NULL;
GO

EXEC spCategoria @Accion=3,
				 @Descripcion='Paletas',
				 @IdCategoria=3
GO

EXEC spCategoria @Accion=4,
				 @IdCategoria=18;
GO

select * from CATEGORIA
